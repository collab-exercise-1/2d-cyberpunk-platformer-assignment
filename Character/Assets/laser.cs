using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser : MonoBehaviour
{
    public float maxtime = 2;
    int order = 1;
    float time;
    // Start is called before the first frame update
    void Start()
    {
        time = maxtime;
    }

    // Update is called once per frame
    public void Update()
    {
        time -= Time.deltaTime;
        if (time < 0 && order == 1)
        {
            order = 2;
            time = maxtime;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 0);
        }
        if (time < 0 && order == 2)
        {
            order = 1;
            time = maxtime;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
            this.gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
        }
    }


}
