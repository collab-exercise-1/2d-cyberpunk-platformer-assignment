using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum FacingDirection { Left, Right }
public class Control : MonoBehaviour
{

        Rigidbody2D rigidbody;

    
    public FacingDirection direction;
    public SpriteRenderer spriteRenderer;
    public float accelerationRate = 10;
        public float decelerationRate = 10;
    public Animator animator;
        public float maxSpeed = 20;
        public float jumpForce = 10;
    public float jetpackForce;
    public float maxtimejet = 0.5f;
    public float jettime = 0.5f;
        public bool ground;
    public int wcount;
    public int jetcount = 1;
    public int maxjet = 2;
    Vector3 spawn;

        public bool canJump;
    public GameObject cam;

    // Start is called before the first frame update
    void Start()
        {
            rigidbody = GetComponent<Rigidbody2D>();
        animator.SetBool("IsJumping", false);


    }

    // Update is called once per frame
    void Update()
    {






        //horizontal movement
        if (Input.GetKey(KeyCode.A))
        {
            animator.SetInteger("isWalking", 2);

            if (jetcount <= 0)
            {
                animator.SetInteger("IsJumpingLeft", 1);
                animator.SetInteger("IsJumpingRight", 0);
            }

            animator.SetInteger("IsBoostingLeft", 1);
            animator.SetInteger("IsBoostingRight", 0);

            if (rigidbody.velocity.x > 0)
            {
                //quick turn from going right to left
                rigidbody.AddForce(Vector2.left * accelerationRate * 5 * Time.deltaTime, ForceMode2D.Impulse);

            }
            //move left
            rigidbody.AddForce(Vector2.left * accelerationRate * Time.deltaTime, ForceMode2D.Impulse);
        }
        else if (Input.GetKey(KeyCode.D))
        {

            animator.SetInteger("isWalking", 1);

            if (jetcount <= 0)
            {
                animator.SetInteger("IsJumpingRight", 1);
                animator.SetInteger("IsJumpingLeft", 0);
            }

            animator.SetInteger("IsBoostingRight", 1);
            animator.SetInteger("IsBoostingLeft", 0);


            if (rigidbody.velocity.x < 0)
            {

                //quick turn from going left to right
                rigidbody.AddForce(Vector2.right * accelerationRate * 5 * Time.deltaTime, ForceMode2D.Impulse);
            }

            rigidbody.AddForce(Vector2.right * accelerationRate * Time.deltaTime, ForceMode2D.Impulse);
            //move right
        }
        else
        {
            if (rigidbody.velocity.x != 0)
            {
                rigidbody.AddForce(new Vector2(-rigidbody.velocity.x, 0) * decelerationRate * Time.deltaTime, ForceMode2D.Impulse);
            }
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            animator.SetInteger("isWalking", 0);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            animator.SetInteger("isWalking", 0);
        }
        //vertical movement




       
        if (Input.GetKeyDown(KeyCode.W) && ground == false && jetcount > 0 && wcount > 0)
        {
           
            rigidbody.AddForce(Vector2.up * jetpackForce, ForceMode2D.Impulse);

            animator.SetBool("IsBoosting", true);
           

            rigidbody.velocity = new Vector2(rigidbody.velocity.x, rigidbody.velocity.y / 8);
        }
        if (Input.GetKey(KeyCode.W) && jetcount > 0 && ground == false && wcount > 0 && jettime > 0)
        {
          
            rigidbody.AddForce(Vector2.up * jetpackForce * Time.deltaTime, ForceMode2D.Impulse);
            jettime -= Time.deltaTime;
           
        }
        if (Input.GetKeyUp(KeyCode.W) && jetcount > 0 && ground == false && wcount > 0)
        {
            jettime = maxtimejet;
            jetcount = jetcount - 1;
        }
        if (Input.GetKeyDown(KeyCode.W) && ground == true)
        {
          
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0);
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            animator.SetBool("IsBoosting", false);





        }
        rigidbody.velocity = Vector2.ClampMagnitude(rigidbody.velocity, maxSpeed);

      

        if (Input.GetKeyUp(KeyCode.W) && wcount == 0)
        {
            wcount = 1;
        }
    }
   

    public void OnLanding()
    {
        
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Checkpoint") //if the trigger area is room two
        {
            spawn = transform.position;
        }

      /*  if (collision.gameObject.name == "Cam Change 1") //if the trigger area is room two
        {
            Vector3 newCamPos = new Vector3(-10.51f, 1.49f, -10); //creates a respawn vector which holds spawn location values
            cam = GameObject.Find("Main Camera");
            cam.transform.position = newCamPos; //change camera position to room two 
        }

        if (collision.gameObject.name == "Cam Change 2")
        {
            Vector3 newCamPos = new Vector3(11.63f, -2.95f, -10); //creates a respawn vector which holds spawn location values
            cam.transform.position = newCamPos; //change camera position to room one
        }

        if (collision.gameObject.name == "Cam Change 3")
        {
            Vector3 newCamPos = new Vector3(8.71f, -12.41f, -10); //creates a respawn vector which holds spawn location values
            cam.transform.position = newCamPos; //change camera position to room three 

        }

        if (collision.gameObject.name == "Cam Change 4")
        {
            Vector3 newCamPos = new Vector3(23.81f, -7.25f, -10); //creates a respawn vector which holds spawn location values
            cam.transform.position = newCamPos; //change camera position to room three 

        }

        if (collision.gameObject.name == "Cam Change 5")
        {
            Vector3 newCamPos = new Vector3(23, 3.26f, -10); //creates a respawn vector which holds spawn location values
            cam.transform.position = newCamPos; //change camera position to room three 

        }
      */
       

        if (collision.gameObject.name == "EndTrigger")
        {
            SceneManager.LoadScene(4); //load victory scene
        }

        if (collision.gameObject.layer == 8)
        {
            jetcount = maxjet;
        }
        else
        {
            spawn = transform.position;
        }





    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 7)
        {
          
            gameObject.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            Invoke("spawnPlayer", .5f);
           
            


        }

       
    }

    public void spawnPlayer()
    {
        gameObject.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        transform.position = spawn;
    }
    public void OnCollisionStay2D(Collision2D collision)
{
    if (collision.gameObject.layer == 6 || collision.gameObject.layer == 9)
    {
        ground = true;
            animator.SetBool("IsJumping", false);

           
            animator.SetInteger("IsJumpingRight", 0);
            animator.SetInteger("IsJumpingLeft", 0);
            animator.SetBool("IsBoosting", false);
            //jetcount = maxjet;
            jettime = maxtimejet;
            wcount = 0;
    }

}

    public void OnCollisionExit2D(Collision2D collision)
    {
        ground = false;
        animator.SetBool("IsJumping", true);
        if (!Input.GetKey(KeyCode.W))
        {
            wcount = 1;
        }
    }


}
