using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{

    public float halfdis = 3;
    float mid;
    public float speed = 2;
    int dir = 1;

    // Start is called before the first frame update
    void Start()
    {
        mid = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > mid + halfdis)
        {
            dir = -1;
        }
        if (transform.position.x < mid - halfdis)
        {
            dir = 1;
        }
        transform.position = new Vector2(transform.position.x + (speed * dir * Time.deltaTime), transform.position.y);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameObject.layer != 7 )
        {
            if (collision.gameObject.tag == "Player")
            {

                collision.collider.transform.SetParent(transform);



            }
        }
        
        }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(null);
    }
}
